﻿namespace NextCallerConnector
{
    /// <summary>
    ///     Used for Discovery. These constants are used to fill in some of the values in the
    ///     ScribeConnectorAttrribute declaration on the class that implements IConnector.
    ///     Connector settings that are often or must different from other connectors.
    /// </summary>
    public class ConnectorSettings
    {
        public const string ConnectorTypeId = "9A5C9ED7-44F6-45D6-8939-5D583DD6836D";

        public const string ConnectorVersion = "1.0.1";

        public const string Description = "Scribe Labs Connector for Next Caller (https://nextcaller.com/).";

        public const string Name = "Scribe Labs – Next Caller";

        public const bool SupportsCloud = true;

        public const string CompanyName = "Scribe Software Corporation";

        public const string AppName = "Scribe Labs – Next Caller";

        public const string Copyright = "Copyright © 2015 Scribe All rights reserved.";
    }
}
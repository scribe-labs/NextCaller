﻿namespace NextCallerConnector
{


    public class Rootobject
    {
        public CallerID[] records { get; set; }
    }

    public class CallerID
    {
        public string id { get; set; }
        public string first_name { get; set; }
        public string first_pronounced { get; set; }
        public string middle_name { get; set; }
        public string last_name { get; set; }
        public string name { get; set; }
        public string language { get; set; }

        public string line_type { get; set; }
        public string carrier { get; set; }
        public string department { get; set; }
        public string resource_uri { get; set; }
    }

    public class Phone
    {
        public string number { get; set; }
        public string resource_uri { get; set; }
    }

    public class Address
    {
        public string city { get; set; }
        public string extended_zip { get; set; }
        public string country { get; set; }
        public string line1 { get; set; }
        public string line2 { get; set; }
        public string state { get; set; }
        public string zip_code { get; set; }
    }


}

﻿namespace NextCallerConnector
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Simple.Connector.Framework.Http;


    using System.Text;

    public class NextCallerConnectionInfo : HttpClientConnectionInfoBase<NextCallerConnectionInfo>
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string BaseUrl { get; set; }


        protected override Func<NextCallerConnectionInfo, IConnectionConfiguration> ConnectionConfiguration()
        {
            return
                this.Connection.ConfigureWithBasicAuth("GET", connInfo => connInfo.BaseUrl, this.Username, this.Password)
                    .ToHeader("Accept", "application/json")
                    .End();
        }

        protected override IList<HttpQueryRegistration> ConfigureQueries()
        {
            string auth = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(Username + ":" + Password));

            var CallerIDGet =
                this.Queries.EnumerateResponseAs<CallerID, Rootobject>("/v2.1/records/", r =>  r.records.Cast<CallerID>())
                    .AcceptJson()
                    .FromInputToQuery("phone")
                    .ToQuery("format", "json")
                    .ToHeader("Authorization", auth);

            return new List<HttpQueryRegistration> { CallerIDGet };
        }

        protected override IList<HttpCallDescription> ConfigureOperations()
        {
                    
            return new List<HttpCallDescription> {  };
        }
    }
}
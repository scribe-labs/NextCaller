﻿namespace NextCallerConnector
{
    using Simple.Connector.Framework;
    using Simple.Connector.Framework.Http;
    

    [SimpleConnector(ConnectorSettings.ConnectorTypeId, ConnectorSettings.Name, ConnectorSettings.Description,
        typeof(Connector), ConnectorSettings.SupportsCloud, ConnectorSettings.ConnectorVersion)]
    public class Connector : HttpConnectorBase<NextCallerConnectionInfo>
    {
        public Connector()
            : base(ConnectorSettings.ConnectorTypeId, ConnectorSettings.CompanyName, "http://www.scribesoft.com", "{F12E35E6-AED9-46BA-B53E-957B310878C7}")
        {
        }
    }
}